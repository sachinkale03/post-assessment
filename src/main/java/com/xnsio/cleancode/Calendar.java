package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.*;

class Calendar {
    private Map<LocalDate, List<Integer>> schedule = new HashMap<>();

    void bookCalendar(LocalDate date, int startHour) {
        List<Integer> hours = getBusyHours(date);
        hours.add(startHour);
        schedule.put(date, hours);
    }

    List<Integer> getBusyHours(LocalDate date) {
        return Optional.ofNullable(schedule.get(date))
                .orElse(new ArrayList<>());
    }

    boolean isBusyOn(LocalDate date, int startHour) {
        return Optional.ofNullable(getBusyHours(date))
                .filter( hrs -> hrs.contains(startHour))
                .isPresent();
    }
}
