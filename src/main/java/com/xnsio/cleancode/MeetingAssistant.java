package com.xnsio.cleancode;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

class MeetingAssistant {
    private TreeSet<Integer> WORKING_HOURS;

    MeetingAssistant(int[] working_hours) {
        WORKING_HOURS = Arrays.stream(working_hours).boxed().collect(Collectors.toCollection(TreeSet::new));
    }

    String assistWith(List<Calendar> calendars, LocalDate endDate, int upToHour) {
        if (isOutsideWorkingHour(upToHour)) return "Meeting can not be schedule outside working hours.";

        for (LocalDate currentDate = LocalDate.now(); currentDate.isBefore(endDate.plusDays(1)); currentDate = currentDate.plusDays(1)) {
            final Integer commonSlot = getFirstAvailableSlot(calendars, currentDate);
            if (canScheduleMeeting(commonSlot, upToHour))
                return "You can schedule meeting at " + commonSlot + " on " + currentDate;
        }
        return "No common time slot available for meeting.";
    }

    private boolean canScheduleMeeting(Integer firstCommonAvailableSlot, int upToHour) {
        return firstCommonAvailableSlot != -1 && firstCommonAvailableSlot < upToHour;
    }

    private Integer getFirstAvailableSlot(List<Calendar> calendars, LocalDate currentDay) {
        TreeSet<Integer> workingHours = new TreeSet<>(WORKING_HOURS);
        workingHours.removeAll(getCommonBusyHours(calendars, currentDay));
        return workingHours.isEmpty() ? -1 : workingHours.first();
    }

    private TreeSet<Integer> getCommonBusyHours(List<Calendar> calendars, LocalDate currentDay) {
        return calendars.stream()
                .flatMap(calendar -> calendar.getBusyHours(currentDay).stream())
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private boolean isOutsideWorkingHour(int hour) {
        return WORKING_HOURS.stream().noneMatch(wh -> wh == hour);
    }
}
