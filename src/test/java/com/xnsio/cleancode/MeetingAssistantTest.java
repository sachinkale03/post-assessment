package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class MeetingAssistantTest {
    private static final LocalDate TODAY = LocalDate.now();
    private static final LocalDate TOMORROW = TODAY.plusDays(1);
    private static final int[] ALL_WORKING_HOURS = {8, 9, 10, 11, 12, 13, 14, 15, 16};

    private final MeetingAssistant meetingAssistance = new MeetingAssistant(ALL_WORKING_HOURS);
    private final Calendar Brad = new Calendar();
    private final Calendar Mini = new Calendar();
    private final Calendar BusyBee = new Calendar();
    private final Calendar Janet = new Calendar();
    private final List<Calendar> calendars = new ArrayList<>();
    private String actualMessage = "";

    @Test
    public void scheduleMeeting() {
        tryToScheduleMeetingWith(Brad)
                .before(10, TODAY)
                .meetingShouldBeScheduledAt(8, TODAY);
    }

    @Test
    public void scheduleMeetingWithBusyPerson() {
        tryToScheduleMeetingWith(Brad)
                .whenBradIsBusy(TODAY, 8, 9)
                .before(10, TODAY)
                .meetingShouldNotBeScheduled("No common time slot available for meeting.");
    }

    @Test
    public void scheduleMeetingWithPersonBusyToday() {
        tryToScheduleMeetingWith(BusyBee)
                .busyBeeIsBusy(TODAY, ALL_WORKING_HOURS).busyBeeIsBusy(TOMORROW, 9)
                .before(16, TOMORROW)
                .meetingShouldBeScheduledAt(8, TOMORROW);
    }

    @Test
    public void scheduleMeetingOutsideWorkingHours() {
        tryToScheduleMeetingWith(Brad)
                .before(17, TODAY)
                .meetingShouldNotBeScheduled("Meeting can not be schedule outside working hours.");
    }

    @Test
    public void scheduleMeetingWithMultiplePeople() {
        tryToScheduleMeetingWith(Brad).and(Janet)
                .whenBradIsBusy(TODAY, 8, 9)
                .janetIsBusy(TODAY, 10)
                .before(12, TODAY)
                .meetingShouldBeScheduledAt(11, TODAY);
    }

    @Test
    public void scheduleMeetingWithSelf() {
        tryToScheduleMeetingWithSelf()
                .whenIAmBusy(TODAY, 8)
                .before(10, TODAY)
                .meetingShouldBeScheduledAt(9, TODAY);
    }

    private MeetingAssistantTest whenIAmBusy(LocalDate date, int busyHour) {
        bookCalendarFor(Mini, date, Collections.singletonList(busyHour));
        return this;
    }

    private MeetingAssistantTest tryToScheduleMeetingWithSelf() {
        calendars.add(Mini);
        return this;
    }

    private MeetingAssistantTest janetIsBusy(LocalDate date, int busyHour) {
        bookCalendarFor(Janet, date, Collections.singletonList(busyHour));
        return this;
    }

    private MeetingAssistantTest busyBeeIsBusy(LocalDate date, int[] busyHours) {
        bookCalendarFor(BusyBee, date, Arrays.stream(busyHours).boxed().collect(Collectors.toList()));
        return this;
    }

    private MeetingAssistantTest busyBeeIsBusy(LocalDate date, int busyHour) {
        bookCalendarFor(BusyBee, date, Collections.singletonList(busyHour));
        return this;
    }

    private MeetingAssistantTest before(int upToTime, LocalDate endDate) {
        actualMessage = meetingAssistance.assistWith(calendars, endDate, upToTime);
        return this;
    }

    private void meetingShouldBeScheduledAt(int time, LocalDate date) {
        assertEquals("You can schedule meeting at " + time + " on " + date, actualMessage);
    }

    private MeetingAssistantTest tryToScheduleMeetingWith(Calendar personsCalendar) {
        calendars.add(Mini);
        calendars.add(personsCalendar);
        return this;
    }

    private MeetingAssistantTest and(Calendar calendar) {
        calendars.add(calendar);
        return this;
    }

    private void bookCalendarFor(Calendar calender, LocalDate date, List<Integer> busyHours) {
        busyHours.forEach(bh -> calender.bookCalendar(date, bh));
    }

    private void meetingShouldNotBeScheduled(String expectedMessage) {
        assertEquals(expectedMessage, actualMessage);
    }

    private MeetingAssistantTest whenBradIsBusy(LocalDate date, int... hours) {
        final List<Integer> busyHours = Arrays.stream(hours).boxed().collect(Collectors.toList());
        bookCalendarFor(Brad, date, busyHours);
        return this;
    }
}
