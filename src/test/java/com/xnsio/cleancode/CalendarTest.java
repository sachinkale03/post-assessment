package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CalendarTest {
    private static final LocalDate TODAY = LocalDate.now();
    private static final int EIGHT_AM = 8;
    private final Calendar calendar = new Calendar();

    @Test
    public void timeSlotShouldBeAvailableWhenCalendarIsNotBooked(){
        assertFalse(calendar.isBusyOn(LocalDate.now(), EIGHT_AM));
    }

    @Test
    public void timeSlotShouldNotBeAvailableWhenCalendarIsBooked() {
        calendar.bookCalendar(TODAY, EIGHT_AM);

        assertTrue(calendar.isBusyOn(TODAY, EIGHT_AM));
    }
}
